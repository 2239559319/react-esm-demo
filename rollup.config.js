const { readFile } = require('fs/promises')
const { join } = require('path');
const { compile } = require('handlebars');
const { defineConfig } = require('rollup');
const ts = require('@rollup/plugin-typescript');
const cjs = require('@rollup/plugin-commonjs');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const html = require('@rollup/plugin-html');

const publicHtmlPath = join(__dirname, './public');

module.exports = defineConfig({
  input: './src/index.ts',
  output: {
    format: 'esm',
    dir: 'dist',
    sourcemap: true,
  },
  plugins: [
    ts(),
    cjs(),
    nodeResolve(),
    html({
      async template({ attributes, files, meta, publicPath, title }) {
        const scripts = (files.js || [])
          .map(({ fileName }) => {
            const attrs = html.makeHtmlAttributes(attributes.script);
            return `<script src="${publicPath}${fileName}"${attrs}></script>`;
          })
          .join('\n');
        const links = (files.css || [])
          .map(({ fileName }) => {
            const attrs = html.makeHtmlAttributes(attributes.link);
            return `<link href="${publicPath}${fileName}" rel="stylesheet"${attrs}>`;
          })
          .join('\n');
        const importmap = await readFile(join(publicHtmlPath, 'importmap.json'), 'utf-8');

        const htmlFile = await readFile(join(publicHtmlPath, 'index.hbs'), 'utf-8');
        const template = compile(htmlFile);

        const res = template({
          links,
          scripts,
          importmap
        });
        return res;
      },
    }),
  ],
  external: [
    'react',
    'react-dom',
    'swr',
    'use-sync-external-store/shim/index.js',
    'use-sync-external-store/with-selector.js',
    '@reduxjs/toolkit',
    'redux',
    'react-redux',
  ],
});
