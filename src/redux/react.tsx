import React from 'react';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { createStore } from 'redux';

const store = createStore((state: any, action) => {
  return {
    ...state,
    num: state.num + 1,
  }
}, {
  num: 1
});


export const Demo = () => {
  return (
    <Provider store={store}>
      <ReduxApp />
    </Provider>
  );
};

const ReduxApp = () => {

  const num = useSelector((state: any) => state.num);

  const dispatch = useDispatch();

  return (
    <div className="redux">
      <div>num is {num}</div>
      <div onClick={() => {
        dispatch({
          type: 'add',
        });
      }}>add</div>
    </div>
  );
};
