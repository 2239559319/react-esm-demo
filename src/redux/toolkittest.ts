import { configureStore } from '@reduxjs/toolkit';

const store = configureStore({
  reducer(state, action) {
    return {
      ...state,
      num: state.num + 1,
    };
  },
  preloadedState: {
    num: 1
  },
});

console.log(store);

