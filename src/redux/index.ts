import { createStore } from 'redux';



const store = createStore<{num: number}, any>((state, action) => {
  return {
    ...state,
    num: state.num + 1,
  };
}, {
  num: 1
});

console.log(store);
