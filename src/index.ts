import { render } from 'react-dom';
import { appElem } from './App';

render(appElem, document.querySelector('#app'));

