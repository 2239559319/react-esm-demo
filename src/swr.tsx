import React from 'react';
import useSWR from 'swr';

export function Demo() {

  const { data, error, isLoading, isValidating, mutate } = useSWR('https://reqres.in/api/users?page=2', async (key) => {
    const res = await fetch(key);
    return res.json();
  });

  return (
    <div className="swr-demo">
      <div>{data ? JSON.stringify(data) : 'data'}</div>
      <div>{error}</div>
      <div>{isLoading}</div>
    </div>
  );
}
