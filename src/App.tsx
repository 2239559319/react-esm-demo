import React from 'react';
import { Demo } from './redux/react';

function App() {
  return (
    <div className="app">
      <div>hello world</div>
      <Demo />
    </div>
  );
}

export const appElem = <App />;
